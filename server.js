const Hapi              = require('hapi');
const AuthBearer        = require('hapi-auth-bearer-token');
const config            = require('./config');
const routes            = require('./routes');

const server            = new Hapi.Server();

server.connection({ port: config.Server.Port });
server.register(AuthBearer, (err) => {

    server.auth.strategy('ekumbu-manager', 'bearer-access-token', {
        allowQueryToken: true,              
        allowMultipleHeaders: true,        
        accessTokenName: 'access_token', 
        validateFunc: function (token, callback) {          
            if (config.manager.superAdmin.superManagerToken != token) {
                return callback(null, false);
            }
            else {
                return callback(null, true, config.manager.superAdmin);
            }
        }
    });

    server.route(routes);
});
server.start();
console.log('Ekumbu Payment Gateway listening at port ' + server.info.port);