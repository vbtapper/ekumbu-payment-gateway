
const TokenController                   = require('./Controllers/TokenController');
const PaymentController                 = require('./Controllers/PaymentController')
const Joi                               = require('joi');

module.exports = [{

    method: 'POST',
    path: '/{merchantkey}/createlivetoken',
    config: {
        auth: {
            strategy: 'ekumbu-manager',
            scope: ['EkumbuAdmin']
        },
        handler: TokenController.CreateLiveToken,
        validate: {
            payload : {
                cardnumber: Joi.string().min(16).max(16),
                expmonth: Joi.string().min(1).max(2),
                expyear: Joi.string().min(4).max(4),
                customerip : Joi.string(),
                type : Joi.string(),
                livemode: Joi.string().min(4).max(5)
            }
        },
        cache: {
            expiresIn: 30 * 1000,
            privacy: 'private'
        }
    }
},
{
    method: 'POST',
    path: '/{merchantkey}/createtesttoken',
    config: {
        auth: {
            strategy: 'ekumbu-manager',
            scope: ['EkumbuAdmin']
        },
        handler: TokenController.CreateTestToken,
        validate: {
            payload : {
                cardnumber: Joi.string().min(16).max(16),
                expmonth: Joi.string().min(1).max(2),
                expyear: Joi.string().min(4).max(4),
                customerip : Joi.string(),
                livemode: Joi.string().min(4).max(5)
            }
        },
        cache: {
            expiresIn: 30 * 1000,
            privacy: 'private'
        }
    }
},
{
    method: 'GET',
    path: '/{merchantkey}/gettoken',
    config: {
        auth: {
            strategy: 'ekumbu-manager',
            scope: ['EkumbuAdmin']
        },
        validate: {
            query: {
                tokenID: Joi.string().required()
            }
        },
        handler: TokenController.RetrieveToken
    }
},
{
    method: 'POST',
    path: '/{merchantkey}/chargetest',
    config: {
        auth: {
            strategy: 'ekumbu-manager',
            scope: ['EkumbuAdmin']
        },
        handler : PaymentController.ChargeTest,
        validate : {
            payload: {
                source: Joi.string().required(),
                amount: Joi.number().required(),
                description: Joi.string().required(),
                livemode: Joi.string().min(4).max(5).required(),
                phoneconfirmation: Joi.string()
            }
        },
        cache: {
            expiresIn: 30 * 1000,
            privacy: 'private'
        }
    }
},
{
    method: 'POST',
    path: '/{merchantkey}/chargelive',
    config: {
        auth: {
            strategy: 'ekumbu-manager',
            scope: ['EkumbuAdmin']
        },
        handler : PaymentController.ChargeLive,
        validate : {
            payload: {
                source: Joi.string(),
                amount: Joi.number(),
                description: Joi.string(),
                livemode: Joi.string().min(4).max(5)
            }
        },
        cache: {
            expiresIn: 30 * 1000,
            privacy: 'private'
        }
    }
},
{
    method: 'POST',
    path: '/{merchantkey}/cptoken',
    config: {
        auth: {
            strategy: 'ekumbu-manager',
            scope: ['EkumbuAdmin']
        },
        handler : PaymentController.CheckPaymentToken,
        validate : {
            payload: {
                paymentid: Joi.string(),
                token: Joi.string(),
                livemode: Joi.string().min(4).max(5)
            }
        },
        cache: {
            expiresIn: 30 * 1000,
            privacy: 'private'
        }
    }
},
{
    method: 'GET',
    path: '/{merchantkey}/getcharge',
    config: {
        auth: {
            strategy: 'ekumbu-manager',
            scope: ['EkumbuAdmin']
        },
        validate: {
            query: {
                chargeID: Joi.string().required()
            }
        },
        handler: PaymentController.RetrieveCharge
    }
},
{
    method: 'GET',
    path: '/{merchantkey}/getallcharge',
    config: {
        auth: {
            strategy: 'ekumbu-manager',
            scope: ['EkumbuAdmin']
        },
        validate: {
            query: {
                livemode: Joi.string().min(4).max(5)
            }
        },
        handler: PaymentController.RetrieveAllCharges
    }
}
]
