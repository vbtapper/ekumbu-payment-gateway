FROM node

WORKDIR /usr/src/app
RUN npm install && npm install -g nodemon

COPY . .

EXPOSE 3014

CMD ["npm", "start"]
CMD ["node", "server.js"]