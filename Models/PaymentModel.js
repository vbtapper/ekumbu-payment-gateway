const Mongoose              = require('mongoose');
const Schema                = Mongoose.Schema;
const db                      = require('../MongoManager/MongoConnection');
const bcrypt                = require('bcryptjs');


const PaymentSchema = new Schema({
    created : { type: Date, default: Date.now },
    status : { type : String, required: true },
    paymentid: { type: String, required: true, unique: true },
    amount: { type : Number, required: true },
    amountWFee : {type: Number},
    confirmed : { type: Boolean, required: true, default: false},
    livemode : {type: Boolean, default: false, required: true},
    conftoken : { type: String, required: true },
    tokenid : { type: String, required: true },
    tokenexp : { type: Boolean, required: true, default: false},
    description : { type: String, required: true },
    fee: { type: Number },
});

var payment = Mongoose.model('payments', PaymentSchema);
module.exports = {
    PaymentSchema: payment
};