const Mongoose              = require('mongoose');
const Schema                = Mongoose.Schema;
const db                      = require('../MongoManager/MongoConnection');
const bcrypt                = require('bcryptjs');

SALT_WORK_FACTOR = 10;

const MerchantSchema = new Schema({
  created: { type: Date, default: Date.now },
  status : {type : Boolean, default: false},
  activationtoken: {type: String},
  activationtokenexpired : {type : Boolean, default: false},
  description : {type: String},
  email: { type: String, index: { unique: true }, required: true },
  password : { type : String, required: true},
  scope: [{type: String}],
  name : { type: String, required: true },
  livemode : {type: Boolean, default: false, required: true},
  apiversion : { type : String },
  keys : {
      live_secret : { type: String, unique: true },
      live_publishable : { type: String, unique: true },
      test_secret : { type: String, unique: true },
      test_publishable : { type: String, unique: true }
   },
   bank_account : {
       verified : {type : Boolean, default: false},
       account_number : {type : String},
       IBAN : {type : String},
       bank_name : {type : String}
   },
   security : {
       active : {type : Boolean, default: false},
       phone_number : {type : String},
       token : { type : String },
       token_expire : { type: Boolean, default: false},
   },
   address : {
       street : {type: String, default: "none"},
       town: {type: String, default: "none"},
       province : {type : String, default: "none"}
   },
   profit : {
       nexttransfer : { type: String, default: "0", required: true },
       lasttransfer : { type: String, default: "0", required: true },
       total : { type: String, default: "0", required: true },
       npayments : { type: Number, default : 0, required: true }
   },
   profittest : {
       nexttransfer : { type: String, default: "0", required: true },
       lasttransfer : { type: String, default: "0", required: true },
       total : { type: String, default: "0", required: true },
       npayments : { type: Number, default : 0, required: true }
   },
   payments : {
       firstlivepayment: {type: Boolean, required: true, default: false}
   }

});

MerchantSchema.pre('save', function(next) {
  //console.log('pre save');
  var merchant = this;
  // only hash the password if it has been modified (or is new)
  if (!merchant.isModified('password')) return next();
  //console.log('before hash');
  bcrypt.hash(merchant.password, SALT_WORK_FACTOR, function(err, hash) {
    //console.log(err);
    if (err) return next(err);
    // override the cleartext password with the hashed one
    merchant.password = hash;
    next();
  });
});

MerchantSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

var merchant = Mongoose.model('merchant', MerchantSchema);
module.exports = {
    MerchantSchema: merchant
};