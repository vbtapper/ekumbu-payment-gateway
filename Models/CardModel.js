const Mongoose              = require('mongoose');
const Schema                = Mongoose.Schema;
var db                      = require('../MongoManager/MongoConnection');

SALT_WORK_FACTOR = 10;

//TODO add field sms confirmation

const CardSchema = new Schema({
    created : { type: Date, default: Date.now },
    status : {type : Boolean, default: false},
    brand: {type : String, required: true, default: "Ekumbu"},
    exp_month : {type : String, required: true},
    exp_year : {type : String, required: true},
    company : {type : String, default: "NossApps"},
    number : {type: String, required: true},
    blocked: {type: Boolean, required: true, default: false},
    daily_payment : { type : Number, default: 0},
    payment_limit : {type : Number, required: true, default: 20000},
    lastpayment : { type: Number, default: 0 },
    model: { type: String, required: true },
    currency : { type : String, required: true, default: "AOA"},
    smsconfirm : {type: Boolean, default: false, required: true},
    cardholder_info : {
        customer_id: {type : String, required: true, unique: true}
    },
    security : {
        secret_token : {type : String, required: true, unique: true},
        cardlive_token : { type : String, required: true, unique: true },
    }
});

CardSchema.pre('save', function(next) {
    next();
});

var card = Mongoose.model('cards', CardSchema);
module.exports = {
    CardSchema: card
};