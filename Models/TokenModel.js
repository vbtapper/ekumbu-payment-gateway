const Mongoose              = require('mongoose');
const Schema                = Mongoose.Schema;
const db                      = require('../MongoManager/MongoConnection');
const bcrypt                = require('bcryptjs');

const TokenSchema = new Schema({
    created : { type: Date, default: Date.now },
    used: { type: Boolean, required: true, default: false },
    livemode : {type: Boolean, default: false, required: true},
    tokenID : { type : String, required: true },
    customer_ip : { type : String, required: true },
    card : { type: Schema.Types.ObjectId, ref: 'Cards' },
    source_info : {
        source_id : { type: String, required: true}
    },
    payment_info: {
        payment_id: {type: String}
    },
    transfer_info: {
        transfer_id: {type: String}
    },
    deposit_info: {
        deposit_id : {type: String}
    }
});

var token = Mongoose.model('tokens', TokenSchema);
module.exports = {
    TokenSchema: token
};