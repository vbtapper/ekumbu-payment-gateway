

const randtoken             = require('rand-token').generator();
const config                = require('../config');
const twilio                = require('twilio');
const mailgun               = require('mailgun-js')({ apiKey: config.mail.mailkey, domain: config.mail.ekumbumail });
const schedule              = require('node-schedule');

const Client                = require('node-rest-client').Client;
const async                 = require("async");
const assert                = require('assert');
const moment                = require('moment');
const Q                     = require('q');
const numeral               = require('numeral');
const formatCurrency        = require('format-currency')

const Customer              = require('../Models/CustomerModel').CustomerSchema;      
const Card                  = require('../Models/CardModel').CardSchema; 
const Token                 = require('../Models/TokenModel').TokenSchema;
const Payment               = require('../Models/PaymentModel').PaymentSchema; 
const Merchant              = require('../Models/MerchantModel').MerchantSchema; 
const CustomEvent           = require('../Models/CustomerEventModel').CustomerEventSchema;


var paymentsResult;
var resInfo;
var errorM;
var SMSClient = new twilio(config.sms.ACCOUNT_SID, config.sms.AUTH_TOKEN);

function GetMerchantTokens(merchantKey, mode) {
    var deferred = Q.defer();
    Token.find({$and: [ { 'source_info.source_id' : merchantKey }, {'livemode' : mode}, {'used' : 'true'}]}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            deferred.resolve(data);
        }
    });
    return deferred.promise;
}

function GetPaymentByID(paymentID, mode) {
    var deferred = Q.defer();
    Payment.findOne({$and: [ { 'paymentid' : paymentID }, {'livemode' : mode}, {'status':'succeded'}]}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            deferred.resolve(data);
        }
    });
    return deferred.promise;
}

function FindMerchantByKey(merchantKey) {
    var deferred = Q.defer();
    Merchant.findOne({ $or: [ { 'keys.live_secret' : merchantKey }, {'keys.test_secret' : merchantKey} ]}, function(err, data) {
        if (err) {
            console.log(err)
            var infoResponse = {
                statusCode: 500,
                error: 'Internal Error',
                message: 'An internal error occurred. Please report it to developers@ekumbu.com'
            }
            deferred.reject(infoResponse)
        }
        else {
            if(data == null || data == undefined) {
                var infoResponse = {
                    statusCode: 403,
                    error: 'Request Error',
                    message: 'invalid source'
                }
                deferred.reject(infoResponse)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    
    return deferred.promise;
}

function GetPaymentByPaymentID(paymentID) {
    var deferred = Q.defer();
    Payment.findOne({"paymentid": paymentID}, function(err, data) {
        if (err) {
            console.log(err)
            var infoResponse = {
                statusCode: 500,
                error: 'Internal Error',
                message: 'An internal error occurred while retrieving the payment ID. Please report it to developers@ekumbu.com'
            }
            deferred.reject(infoResponse)
        }
        else {
            if(data == null || data == undefined) {
                var infoResponse = {
                    statusCode: 403,
                    error: 'Request Error',
                    message: 'payment id not found'
                }
                deferred.reject(infoResponse)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

function GetTokenByID(tokenID, livemodeParam) {
    var deferred = Q.defer();
    Token.findOne({ $and: [ { 'tokenID' : tokenID }, {'livemode' : livemodeParam} ]}, function(err, data) {
        if (err) {
            console.log(err)
            var msgResponse = {
                statusCode: 500,
                error : 'Internal Error',
                message: 'An internal error occurred. Please contact developers@ekumbu.com to report'
            }
            deferred.reject(msgResponse)
        }
        else {
            if(data == null || data == undefined) {
                var msgResponse = {
                    statusCode: 403,
                    error : 'Request Error',
                    message: 'Invalid Token ID'
                }
                deferred.reject(msgResponse)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    
    return deferred.promise;
}

function GetCardByID(cardID) {
    var deferred = Q.defer();
    Card.findOne({"_id": cardID}, function(err, data) {
        if (err) {
            console.log(err)
            var msgResponse = {
                statusCode: 500,
                error : 'Internal Error',
                message: 'An internal error occurred. Please contact developers@ekumbu.com to report'
            }
            deferred.reject(msgResponse)
        }
        else {
            if(data == null || data == undefined) {
                var msgResponse = {
                    statusCode: 500,
                    error : 'Internal Error',
                    message: 'An internal error occurred. Please contact developers@ekumbu.com to report'
                }
                deferred.reject(msgResponse)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

function CheckCustomersFunds(tokenp, amount) {
    var deferred = Q.defer();

    var client = new Client();
    var args = {
        headers: { 
            "Content-Type": "application/json",
        },
        parameters: {
            token: tokenp
        },
    };
    client.get(config.vendor.url + '/chkbalance', args, function (data, response) {
        if(data['statusCode'] == 200) {
            var amountToChk = parseFloat(amount)
            var availableAmount = parseFloat(data['balance'])
            if(amountToChk > availableAmount) {
                deferred.reject(data);
            }
            else {
                deferred.resolve(data);
            }
        } 
        else {
            deferred.reject(data);
        }
    });
    
    return deferred.promise;
}

function CreatePayment(tokenID, mode, amountp, description) {
    var deferred = Q.defer();

    var newPaymentInfo = {
        status : "Processing",
        livemode: mode,
        paymentid : "chr_" + randtoken.generate(20, config.charge.chargeTokenID),
        amount : amountp,
        conftoken : randtoken.generate(6, config.charge.confirmationtoken),
        tokenid : tokenID,
        description : description,
    }

    var newPaym = new Payment(newPaymentInfo)
    newPaym.save(function(err, data) {
        if (err) {
            deferred.reject(err)
        }
        else {
            deferred.resolve(data)
        }
    });

    return deferred.promise;
}

function GetCustomerByID(customerID) {
    var deferred = Q.defer();
    Customer.findOne({"_id": customerID}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(data)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

function GetMerchantByID(merchantID) {
    var deferred = Q.defer();
    Merchant.findOne({"_id": merchantID}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(data);
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

function ChargeEkumbuAccount(tokenp, amount) {

    var deferred = Q.defer();

    var client = new Client();
    var args = {
        headers: { 
            "Content-Type": "application/json",
        },
        data: {
            token : tokenp,
            amount: amount.toString()
        },
    };
    client.post(config.vendor.url + '/charge', args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data);
        } 
        else {
            deferred.reject(data);
        }
    });
    
    return deferred.promise;
}

function EditMerchantStatsByID(merchID, modeParam, amount) {
    var deferred = Q.defer();
    Merchant.findOne({"_id": merchID}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            var amountToAdd = parseFloat(amount);
            if(modeParam.toString().trim() === 'true') {              
                var newnt = (parseFloat(data.profit.nexttransfer)+amountToAdd).toString();
                data.profit.nexttransfer=newnt;

                var newtt = (parseFloat(data.profit.total)+amountToAdd).toString();
                data.profit.total=newtt
                data.profit.npayments+=1;
            }
            else {
                var newntt = (parseFloat(data.profittest.nexttransfer)+amountToAdd).toString();
                data.profittest.nexttransfer = newntt;

                var newttt = (parseFloat(data.profittest.total)+amountToAdd).toString();                
                data.profittest.total=newttt;
                data.profittest.npayments+=1;
            }

            data.save();
            deferred.resolve(data);
        }
    });
    return deferred.promise;
}

function CreateCustomerEvent(customerID, amountA, merchName, merchID, paymentID) {
    var deferred = Q.defer();

    Date.prototype.monthNames = [
        "Janeiro", "Fevereiro", "Março",
        "Abril", "Maio", "Junho",
        "Julho", "Agosto", "Setembro",
        "Octubro", "Novembro", "Dezembro"
    ];

    Date.prototype.getMonthName = function() {
        return this.monthNames[this.getMonth()];
    };
    Date.prototype.getShortMonthName = function () {
        return this.getMonthName().substr(0, 3);
    };
    var dt = new Date();

    var hours = dt.getHours();
    var mins = dt.getMinutes();
    var day = dt.getDate();
    var month = dt.getShortMonthName();

    var newCeventInfo = {
        type: "payment",
        amount: amountA,
        customer_id: customerID,
        hour: hours + ":" + mins,
        date: month + " " + day,
        source_info : {
            name: merchName,
            sourceid: merchID
        },
        details : {
            id : paymentID
        }
    }

    var newCustEvt = new CustomEvent(newCeventInfo)
    newCustEvt.save(function(err, data) {
        if (err) {
            deferred.reject(err)
        }
        else {
            deferred.resolve(data)
        }
    });

    return deferred.promise;
}

function RetrieveToken(tokenIDParam, merchantIDParam) {
    var deferred = Q.defer();
    Token.findOne({$and: [ { 'tokenID' : tokenIDParam }, {'source_info.source_id' : merchantIDParam} ]}, function(error, data){
        if (error) {
            var InfoResponse = {
                statusCode: 500,
                error: 'Internal Error',
                message: 'an internal error occurred while retrieving the token, please contact developers@ekumbu.com to report'
            }
            deferred.reject(InfoResponse);
        }
        else {
            if(data == null || data == undefined) {
                var InfoResponse = {
                    statusCode: 404,
                    error: 'Not Found',
                    message: 'token not found'
                }
                deferred.reject(InfoResponse)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;   
}

function FindCardByID(cardIDParam) {
    var deferred = Q.defer();
    Card.findOne({"_id": cardIDParam}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    
    return deferred.promise;
}

function GetPaymentsByMerchantID(modeParam) {
    var deferred = Q.defer();
    Payment.find({"livemode" : modeParam}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            deferred.resolve(data);
        }
    })
    return deferred.promise;
}

module.exports = {
    ChargeTest: function(request, reply) {
        var msgResponse;
        GetTokenByID(request.payload.source, request.payload.livemode)
        .then(token => {
            GetCardByID(token.card)
            .then(card => {
                CheckCustomersFunds(card.security.secret_token, request.payload.amount)
                .then(dataFounds => {
                    var floatAmount = parseFloat(request.payload.amount)
                    CreatePayment(token.tokenID, request.payload.livemode, floatAmount, request.payload.description)
                    .then(payment => {
                        GetCustomerByID(card.cardholder_info.customer_id)
                        .then(customer => {
                            GetMerchantByID(token.source_info.source_id) 
                            .then(merchant => {
                                if(card.smsconfirm) {
                                    if(request.payload.phoneconfirmation == null || request.payload.phoneconfirmation == undefined) {
                                        var msgResponse = {
                                            statusCode: 403,
                                            error: 'Request Error',
                                            message: 'Please provide the number to receive the confirmation token'
                                        }
                                        return reply(msgResponse)
                                    }
                                    else {
                                        let startTime = new Date(Date.now()+(10 * 60 * 1000));
                                        var j = schedule.scheduleJob(startTime , function(){
                                            j.cancel();
                                            payment.tokenexp = true;
                                            payment.save();
                                        });
                                        SMSClient.messages.create({
                                            to: request.payload.phoneconfirmation,              // Any number Twilio can deliver to
                                            from: '+12109085592',               // A number you bought from Twilio and can use for outbound communication
                                            body: 'Autorize a loja ' + merchant.name + ' a retirar ' + payment.amount + ' Kzs do seu cartão ekumbu usando o código ' + payment.conftoken        // body of the SMS message
                                        })
                                        .then(message => {
                                            console.log(message.sid)
                                            msgResponse = {
                                                statusCode: 200,
                                                error : null,
                                                message: 'Authorization Required',
                                                paymentid: payment.paymentid
                                            }
                                            return reply(msgResponse);
                                        });   
                                    }
                                }
                                else {
                                    ChargeEkumbuAccount(card.security.secret_token, payment.amount)
                                    .then(data => {

                                        var fee = parseFloat(payment.amount*config.charge.fee)/100;
                                        payment.fee = fee
                                        payment.amountWFee = parseFloat(payment.amount-fee);
                                        payment.status = "succeded";
                                        payment.confirmed = true;
                                        payment.tokenexp = true;
                                        payment.save();

                                        token.used = true;
                                        token.payment_info.payment_id = payment.paymentid;
                                        token.save();

                                        EditMerchantStatsByID(token.source_info.source_id, request.payload.livemode, payment.amountWFee)
                                        .then(MerchantStats => {
                                                let opts = { format: '%v %c', code: 'AOA' }
                                                var formatedTotal = formatCurrency(payment.amount, opts);
                                                CreateCustomerEvent(customer._id, formatedTotal, MerchantStats.name, MerchantStats._id, payment.paymentid)
                                                .then(CustomerEvent => {
                                                    var msgResponse = {
                                                        statusCode: 200,
                                                        error : null,
                                                        message: 'success',
                                                        paymentid: payment.paymentid
                                                    }
                                                    return reply(msgResponse)
                                                })
                                                .catch(err => {
                                                    console.log(err)
                                                    msgResponse = {
                                                        statusCode: 500,
                                                        error : 'Internal Error',
                                                        message: 'An internal error occured while creating customer record, but the payment was processed. Please contact devsupport@ekumbu.com'
                                                    }
                                                    return reply(msgResponse)
                                                })
                                            })
                                            .catch(err => {
                                                console.log(err)
                                                msgResponse = {
                                                    statusCode: 500,
                                                    error : 'Internal Error',
                                                    message: 'An internal error occured while editing merchant statistics, but the payment was processed. Please contact devsupport@ekumbu.com'
                                                }
                                                return reply(msgResponse)
                                            })
                                    })
                                    .catch(err => {
                                        console.log(err)
                                        msgResponse = {
                                            statusCode: 403,
                                            error : 'Request Error',
                                            message: "An error occured trying trasnfer the funds."
                                        }
                                        return reply(msgResponse)
                                    })
                                }           
                            })
                            .catch(err => {
                                console.log(err);
                                msgResponse = {
                                    statusCode: 500,
                                    error : 'Internal Error',
                                    message: 'An internal error occured while saving payment. Please contact devsupport@ekumbu.com'
                                }
                                return reply(msgResponse);
                            })   
                        })
                        .catch(err => {
                            console.log(err);
                            msgResponse = {
                                statusCode: 500,
                                error : 'Internal Error',
                                message: 'An internal error occured while saving payment. Please contact devsupport@ekumbu.com'
                            }
                            return reply(msgResponse);
                        })                        
                    })
                    .catch(err => {
                        console.log(err);
                        msgResponse = {
                            statusCode: 500,
                            error : 'Internal Error',
                            message: 'An internal error occured while saving payment. Please contact devsupport@ekumbu.com'
                        }
                        return reply(msgResponse);
                    })                    
                })
                .catch(err => {
                    console.log(err);
                    msgResponse = {
                        statusCode: 403,
                        error : 'Request Error',
                        message: 'decline'
                    }
                    return reply(msgResponse);
                })
            }).catch(err => {
                console.log(err);
                return reply(err);
            })
        })
        .catch(err => {
            console.log(err);
            return reply(err);
        })
        .done();
    },

    ChargeLive: function(request, reply) {
        var msgResponse;
        GetTokenByID(request.payload.source, request.payload.livemode)
        .then(token => {
            GetCardByID(token.card)
            .then(card => {
                CheckCustomersFunds(card.security.secret_token, request.payload.amount)
                .then(dataFounds => {
                    var floatAmount = parseFloat(request.payload.amount)
                    CreatePayment(token.tokenID, request.payload.livemode, floatAmount, request.payload.description)
                    .then(payment => {
                        GetCustomerByID(card.cardholder_info.customer_id)
                        .then(customer => {
                            GetMerchantByID(token.source_info.source_id) 
                            .then(merchant => {
                                if(card.smsconfirm) {
                                    let startTime = new Date(Date.now()+(10 * 60 * 1000));
                                    var j = schedule.scheduleJob(startTime , function(){
                                        j.cancel();
                                        payment.tokenexp = true;
                                        payment.save();
                                    });
                                    SMSClient.messages.create({
                                        to: customer.phone,              // Any number Twilio can deliver to
                                        from: '+12109085592',               // A number you bought from Twilio and can use for outbound communication
                                        body: 'Autorize a loja ' + merchant.name + ' a retirar ' + payment.amount + ' Kzs do seu cartão ekumbu usando o código ' + payment.conftoken        // body of the SMS message
                                    })
                                    .then(message => {
                                        console.log(message.sid)
                                        msgResponse = {
                                            statusCode: 200,
                                            error : null,
                                            message: 'Authorization Required',
                                            paymentid: payment.paymentid
                                        }
                                        return reply(msgResponse);
                                    }); 
                                }
                                else {
                                    ChargeEkumbuAccount(card.security.secret_token, payment.amount)
                                    .then(data => {

                                        var fee = parseFloat(payment.amount*config.charge.fee)/100;
                                        payment.fee = fee
                                        payment.amountWFee = parseFloat(payment.amount-fee);
                                        payment.status = "succeded";
                                        payment.confirmed = true;
                                        payment.tokenexp = true;
                                        payment.save();

                                        token.used = true;
                                        token.payment_info.payment_id = payment.paymentid;
                                        token.save();

                                        EditMerchantStatsByID(token.source_info.source_id, request.payload.livemode, payment.amountWFee)
                                        .then(MerchantStats => {
                                                let opts = { format: '%v %c', code: 'AOA' }
                                                var formatedTotal = formatCurrency(payment.amount, opts);
                                                CreateCustomerEvent(customer._id, formatedTotal, MerchantStats.name, MerchantStats._id, payment.paymentid)
                                                .then(CustomerEvent => {
                                                    msgResponse = {
                                                        statusCode: 200,
                                                        error : null,
                                                        message: 'success'
                                                    }
                                                    return reply(msgResponse)
                                                })
                                                .catch(err => {
                                                    console.log(err)
                                                    msgResponse = {
                                                        statusCode: 500,
                                                        error : 'Internal Error',
                                                        message: 'An internal error occured while creating customer record, but the payment was processed. Please contact devsupport@ekumbu.com'
                                                    }
                                                    return reply(msgResponse)
                                                })
                                            })
                                            .catch(err => {
                                                console.log(err)
                                                msgResponse = {
                                                    statusCode: 500,
                                                    error : 'Internal Error',
                                                    message: 'An internal error occured while editing merchant statistics, but the payment was processed. Please contact devsupport@ekumbu.com'
                                                }
                                                return reply(msgResponse)
                                            })
                                    })
                                    .catch(err => {
                                        console.log(err)
                                        msgResponse = {
                                            statusCode: 403,
                                            error : 'Request Error',
                                            message: "An error occured trying trasnfer the funds."
                                        }
                                        return reply(msgResponse)
                                    })
                                }           
                            })
                            .catch(err => {
                                console.log(err);
                                msgResponse = {
                                    statusCode: 500,
                                    error : 'Internal Error',
                                    message: 'An internal error occured while saving payment. Please contact devsupport@ekumbu.com'
                                }
                                return reply(msgResponse);
                            })   
                        })
                        .catch(err => {
                            console.log(err);
                            msgResponse = {
                                statusCode: 500,
                                error : 'Internal Error',
                                message: 'An internal error occured while saving payment. Please contact devsupport@ekumbu.com'
                            }
                            return reply(msgResponse);
                        })                        
                    })
                    .catch(err => {
                        console.log(err);
                        msgResponse = {
                            statusCode: 500,
                            error : 'Internal Error',
                            message: 'An internal error occured while saving payment. Please contact devsupport@ekumbu.com'
                        }
                        return reply(msgResponse);
                    })                    
                })
                .catch(err => {
                    console.log(err);
                    msgResponse = {
                        statusCode: 403,
                        error : 'Request Error',
                        message: 'decline'
                    }
                    return reply(msgResponse);
                })
            }).catch(err => {
                console.log(err);
                return reply(err);
            })
        })
        .catch(err => {
            console.log(err);
            return reply(err);
        })
        .done();
    },

    CheckPaymentToken: function(request, reply) {
        GetPaymentByPaymentID(request.payload.paymentid)
        .then(payment => {
            if(payment.conftoken != request.payload.token) {
                rspMessage = {
                    statusCode: 403,
                    error : 'Request Error',
                    message: 'Invalid Confirmation Token'
                }
                return reply(rspMessage)
            }
            if(payment.tokenexp) {
                rspMessage = {
                    statusCode: 403,
                    error : 'Request Error',
                    message: 'Confirmation Token Expired'
                }
                return reply(rspMessage)
            }
            GetTokenByID(payment.tokenid, request.payload.livemode)
            .then(Token => {

                GetCardByID(Token.card)
                .then(Card => {
                    
                    ChargeEkumbuAccount(Card.security.secret_token, payment.amount)
                    .then(data => {

                        var fee =  (parseFloat(payment.amount)*config.charge.fee)/100;
                        payment.fee = fee
                        payment.amountWFee = "" + parseFloat(payment.amount) - fee;
                        payment.status = "succeded";
                        payment.confirmed = true;
                        payment.tokenexp = true;
                        payment.save();

                        Token.used = true;
                        Token.payment_info.payment_id = payment.paymentid;
                        Token.save();

                        EditMerchantStatsByID(Token.source_info.source_id, request.payload.livemode, payment.amountWFee)
                        .then(Merchant => {
                            GetCustomerByID(Card.cardholder_info.customer_id)
                            .then(Customer => {
                                let opts = { format: '%v %c', code: 'AOA' }
                                var formatedTotal = formatCurrency(payment.amount, opts);
                                CreateCustomerEvent(Customer._id, formatedTotal, Merchant.name, Merchant._id, payment.paymentid)
                                .then(CustomerEvent => {
                                    var rspMessage = {
                                        statusCode: 200,
                                        error : null,
                                        message: 'success',
                                        paymentid: payment.paymentid
                                    }
                                    return reply(rspMessage)
                                })
                                .catch(err => {
                                    console.log(err)
                                    rspMessage = {
                                        statusCode: 500,
                                        error : 'Internal Error',
                                        message: 'An internal error occured while creating customer record, but the payment was processed. Please contact devsupport@ekumbu.com'
                                    }
                                    return reply(rspMessage)
                                })
                            })
                            .catch(err => {
                                console.log(err)
                                rspMessage = {
                                    statusCode: 500,
                                    error : 'Internal Error',
                                    message: 'An internal error occured, but the payment was processed. Please contact devsupport@ekumbu.com'
                                }
                                return reply(rspMessage)  
                            })
                        })
                        .catch(err => {
                            console.log(err)
                            rspMessage = {
                                statusCode: 500,
                                error : 'Internal Error',
                                message: 'An internal error occured while editing merchant statistics, but the payment was processed. Please contact developers@ekumbu.com'
                            }
                            return reply(rspMessage)
                        })
                    })
                    .catch(err => {
                        console.log(err)
                        return reply(err)
                    })
                })
                .catch(err => {
                    console.log(err)
                    rspMessage = {
                        statusCode: 500,
                        error : 'Internal Error',
                        message: 'An internal error occured while processing payment. Please contact developers@ekumbu.com'
                    }
                    return reply(rspMessage)
                })
            })
            .catch(err => {
                console.log(err)
                rspMessage = {
                    statusCode: 500,
                    error : 'Internal Error',
                    message: 'An internal error occured while processing payment. Please contact developers@ekumbu.com'
                }
                return reply(rspMessage)
            })
        })
        .catch(err => {
            console.log(err)
            return reply(err)
        })
    },

    RetrieveCharge: function(request, reply) {
        FindMerchantByKey(request.params.merchantkey)
        .then(FoundMerchant => {
            GetPaymentByPaymentID(request.query.chargeID)
            .then(FoundCharge => {
                RetrieveToken(FoundCharge.tokenid, FoundMerchant._id)
                .then(FoundToken => {
                    FindCardByID(FoundToken.card)
                    .then(FoundCard => {
                        var InfoResponse = {
                            statusCode: 200,
                            error: null,
                            message: 'success',
                            id: FoundCharge.paymentid,
                            card: {
                                last4: FoundCard.number.substr(FoundCard.number.length -4, FoundCard.number.length),
                                exp_month: FoundCard.exp_month,
                                exp_year: FoundCard.exp_year
                            },
                            customerIP: FoundToken.customer_ip,
                            tokenID: FoundToken.tokenID,
                            created: FoundCharge.created,
                            description: FoundCharge.description,
                            confirmed: FoundCharge.confirmed,
                            amount : FoundCharge.amount,
                            currency: 'AOA'
                        }
                        return reply(InfoResponse)
                    })
                    .catch(error => {
                        console.log(error)
                        var InfoResponse = {
                            statusCode: 500,
                            error: 'Internal Error',
                            message: 'an internal error occurred while retrieving the token, please contact developers@ekumbu.com to report'
                        }
                        deferred.reject(InfoResponse);
                    })
                })
                .catch(error => {
                    console.log(error)
                    return reply(error)
                })
            })
            .catch(error => {
                console.log(error)
                return reply(error)
            })
        })
        .catch(err => {
            console.log(err)
            return reply(err);
        }).done();
    },

    RetrieveAllCharges: function(request, reply) {

        var result = [];
        
        function PushToResult(Payment, Card, Token) {
             result.push({
                id: Payment.paymentid,
                card: {
                    last4: Card.number.substr(Card.number.length-4, Card.number.length),
                    exp_month: Card.exp_month,
                    exp_year: Card.exp_year
                },
                customerIP: Token.customer_ip,
                tokenID: Token.tokenID,
                created: Payment.created,
                description: Payment.description,
                confirmed: Payment.confirmed,
                amount : Payment.amount,
                amountWithFee: Payment.amountWFee,
                currency: 'AOA'
            })
        }
        FindMerchantByKey(request.params.merchantkey)
        .then(FoundMerchant => {
            GetMerchantTokens(FoundMerchant._id, request.query.livemode)
            .then(Tokens =>{
                async.forEach(Tokens, function (element, callback){ 
                    GetPaymentByID(element.payment_info.payment_id, request.query.livemode)
                    .then(payment => {
                        GetCardByID(element.card) 
                        .then(card => {
                            PushToResult(payment, card, element); 
                            callback()
                        })
                    })
                }, function(err) {
                    rspMessage = {
                        statusCode: 200,
                        error: null,
                        message: 'success',
                        payments: result
                    }
                    return reply(rspMessage)
                });  
            })
            .catch(err => {
                console.log(err);
                rspMessage = {
                    statusCode: 500,
                    error: 'Internal Error',
                    message: 'An internal error occured',
                }
                return reply(rspMessage)
            })
        })
        .catch(error => {
            console.log(error)
            return reply(error)
        })
    }
}