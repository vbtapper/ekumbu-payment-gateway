
const randtoken               = require('rand-token').generator();
const config                  = require('../config');
const moment                  = require('moment');

const Card                    = require('../Models/CardModel').CardSchema; 
const Token                   = require('../Models/TokenModel').TokenSchema; 
const Merchant                = require('../Models/MerchantModel').MerchantSchema;
const Q                       = require('q');

function FindMerchantByKey(merchantKey) {
    var deferred = Q.defer();
    Merchant.findOne({ $or: [ { 'keys.live_secret' : merchantKey }, {'keys.test_secret' : merchantKey} ]}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    
    return deferred.promise;
}

function isEmptyObject(obj) {
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      return false;
    }
  }
  return true;
}

function FindCard(cardNumber, cardExpMonth, cardExpYear, model) {
    var deferred = Q.defer();
    Card.findOne({$and: [ { 'number' : cardNumber }, {'exp_month' : cardExpMonth}, {'exp_year': cardExpYear}, {'model' : model} ]}, function(error, data){
        if (error) {
            deferred.reject(new Error(error));
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;
}

function CreateToken(payload, Card, merchID) {
    var deferred = Q.defer();

    var newTokenInfo = {
        type : payload.type,
        livemode : payload.livemode,
        customer_ip : payload.customerip,
        tokenID: "tok_" + randtoken.generate(20, config.tokens.tokenIDSequence),
        card : Card._id,
        source_info: {
            source_id : merchID
        }
    }
    
    var newToken = new Token(newTokenInfo)
    newToken.save(function(err, data) {
        if (err) {
            deferred.reject(err)
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(data)
            }
            else {
                deferred.resolve(data)
            }
        }
    });

    return deferred.promise;
}

function RetrieveToken(tokenIDParam, merchantIDParam) {
    var deferred = Q.defer();
    Token.findOne({$and: [ { 'tokenID' : tokenIDParam }, {'source_info.source_id' : merchantIDParam} ]}, function(error, data){
        if (error) {
            var InfoResponse = {
                statusCode: 500,
                error: 'Internal Error',
                message: 'an internal error occurred while retrieving the token, please contact developers@ekumbu.com to report'
            }
            deferred.reject(InfoResponse);
        }
        else {
            if(data == null || data == undefined) {
                var InfoResponse = {
                    statusCode: 404,
                    error: 'not Found',
                    message: 'token not found'
                }
                deferred.reject(InfoResponse)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    return deferred.promise;   
}

function FindCardByID(cardIDParam) {
    var deferred = Q.defer();
    Card.findOne({"_id": cardIDParam}, function(err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            if(data == null || data == undefined) {
                deferred.reject(null)
            }
            else {
                deferred.resolve(data);
            }
        }
    });
    
    return deferred.promise;
}

module.exports = {
    CreateLiveToken: function(request, reply) {
        FindMerchantByKey(request.params.merchantkey)
        .then(merch => {
            if(merch.bank_account.verified === false) {
                responseData = {
                    statusCode : 403,
                    error : 'Request Error',
                    message : 'bank account not verified',
                }
                return reply(responseData);
            }
            else {
                FindCard(request.payload.cardnumber, request.payload.expmonth, request.payload.expyear, "ekumbu-virtual")
                .then(card => {   
                    var dateNow = new Date(Date.now());
                    var cardExpDate = new Date("01/" + card.exp_month + "/" + card.exp_year)
                    if(moment(cardExpDate).isBefore(dateNow)) {
                        var responseData = {
                            statusCode: 403,
                            error: 'Request Error',
                            message: 'card expired'
                        }
                        return reply(responseData)
                    }
                    else {
                        CreateToken(request.payload, card, merch._id)
                        .then(token => {
                            responseData = {
                                statusCode : 200,
                                error : null,
                                message : 'success',
                                tokenid : token.tokenID
                            }
                            return reply(responseData);
                        })
                        .catch(err => {
                            console.log(err)
                            responseData = {
                                statusCode : 500,
                                error : 'Internal Error',
                                message : 'An internal error occurred while creating the token'
                            }
                            return reply(responseData);
                        })
                        .done();  
                    }
                }).catch(err => {
                    console.log('Invalid Card Info - ' + err)
                    responseData = {
                        statusCode : 403,
                        error : 'Request Error',
                        message : 'Invalid Card Info'
                    }
                    return reply(responseData);
                }).done();
            } 
        }).catch(err => {
            responseData = {
                statusCode : 403,
                error : 'Request Error',
                message : 'invalid source'
            }
            return reply(responseData);
        }).done();
    },
    
    CreateTestToken: function(request, reply) {
        FindMerchantByKey(request.params.merchantkey)
        .then(merch => {
            FindCard(request.payload.cardnumber, request.payload.expmonth, request.payload.expyear, "ekumbu-test")
            .then(card => {   
                var dateNow = new Date(Date.now());
                var cardExpDate = new Date("01/" + card.exp_month + "/" + card.exp_year)
                if(moment(cardExpDate).isBefore(dateNow)) {
                    var responseData = {
                        statusCode: 403,
                        error: 'Request Error',
                        message: 'card expired'
                    }
                    return reply(responseData)
                }
                else {
                    CreateToken(request.payload, card, merch._id)
                    .then(token => {
                        responseData = {
                            statusCode : 200,
                            error : null,
                            message : 'success',
                            tokenid : token.tokenID
                        }
                        return reply(responseData);
                    })
                    .catch(err => {
                        console.log(err)
                        responseData = {
                            statusCode : 500,
                            error : 'Internal Error',
                            message : 'An internal error occured while creating the token'
                        }
                        return reply(responseData);
                    })
                    .done();  
                }
            }).catch(err => {
                console.log('card info not found ' + err)
                responseData = {
                    statusCode : 403,
                    error : 'Request Error',
                    message : 'invalid card information'
                }
                return reply(responseData);
            }).done();
        })
        .catch(err => {
            console.log('invalid source ' + err)
            responseData = {
                statusCode : 403,
                error : 'Request Error',
                message : 'invalid source'
            }
            return reply(responseData);
        }).done();
    },

    RetrieveToken: function(request, reply) {
        FindMerchantByKey(request.params.merchantkey)
        .then(FoundMerchant => {
            RetrieveToken(request.query.tokenID, FoundMerchant._id)
            .then(FoundToken => {
                FindCardByID(FoundToken.card)
                .then(FoundCard => {
                    var InfoResponse = {
                        statusCode: 200,
                        error: null,
                        message: 'success',
                        id: FoundToken.tokenID,
                        card: {
                            last4: FoundCard.number.substr(FoundCard.number.length -4, FoundCard.number.length),
                            exp_month: FoundCard.exp_month,
                            exp_year: FoundCard.exp_year
                        },
                        customer_ip: FoundToken.customer_ip,
                        created: FoundToken.created,
                        used: FoundToken.used
                    }
                    return reply(InfoResponse)
                })
                .catch(error => {
                    console.log(error)
                    var InfoResponse = {
                        statusCode: 500,
                        error: 'Internal Error',
                        message: 'an internal error occurred while retrieving the token, please contact developers@ekumbu.com to report'
                    }
                    deferred.reject(InfoResponse);
                })
            })
            .catch(error => {
                console.log(error)
                return reply(error)
            })
        })
        .catch(err => {
            responseData = {
                statusCode : 403,
                error : 'Request Error',
                message : 'invalid source'
            }
            return reply(responseData);
        }).done();
    }
}