module.exports = {
    Server: {
        Port: 3014
    },
    manager : {
        superAdmin: {
            scope: ['EkumbuAdmin'],
            superManagerToken: 'payment-access-token'
        }
    },
    vendor : {
        devUrl: 'http://localhost:3002',
        url : 'http://stagingapivendor.ekumbu.com'
    },
    deposit: {
        depositTokenID : "435324dsfsd32423SFDSFFDS23432423DSXF32432SDSDWSADSADasdasr32423sdadSDFDSF",
        trackingID : "6adkA453kuA4wydhsadB4dfasj2SDFdcsdkd34jadsgAWGDJV43422scsd2234cndLPCmxapzqDSF453weasdDAd",
        fee: 6
    },
    sms: {
        ACCOUNT_SID: "ACccf82021a95d0d43920fc74def9568a5",
        AUTH_TOKEN: "390ee01e6102c4ba5fccd5e922dd0274"
    },
    mail: {
        ekumbumail : "mail.ekumbu.com",
        mailkey : "key-6c9a2780e8f0a9a0976bd6938e0ffb23"
    },
    charge : {
        chargeTokenID : "XF32432SDSDWSADSADasdasr32423sdadSDFDSF435324dsfsd32423SFDSFFDS23432423DS",
        confirmationtoken : "4849110304923403219324832489324390248329041284320948329047242098234329048203",
        fee: 2.9
    },
    tokens : {
        tokenIDSequence: '01234567890987654321'
    }
}